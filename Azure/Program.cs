﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Azure
{
    class Program
    {
        static void RunOnBackground()
        {
            Thread.Sleep(500);
            Console.WriteLine("Háttérszál véget ért.");
        }


        static void Main(string[] args)
        {
            ThreadsAndTasks();

            DateTime dt = new DateTime(2012, 02, 01);

            //dt = new DateTime(dt.Year, dt.Month, dt.Day + 30);
            dt = dt.AddDays(30);
            Console.ReadKey();
        }

        private static async void ThreadsAndTasks()
        {
            // Régen szálak létrehozása:
            var thread = new System.Threading.Thread(RunOnBackground);
            thread.Start();
            Console.WriteLine("Ezt szerettem volna, ha a THREAD végeztével írja ki...");

            // Most taskok létrehozása:
            var task = Task.Factory.StartNew(RunOnBackground);
            await task;
            Console.WriteLine("Ezt szerettem volna, ha a TASK végeztével írja ki...");
        }
    }
}
