﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace UserInterfaceDemo
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            Loaded += MainPage_Loaded;
        }

        private async void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            // 4. indexű sor
            OuterGrid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });

            var text = new TextBlock
            {
                Text = "itt egy szöveg",
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
            };
            OuterGrid.Children.Add(text);
            Grid.SetRow(text, 4);
            Grid.SetColumnSpan(text, 2); //lógjon át
        }

        // ide navigálódik az alkalmazás == onappearing (XF)
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
        }

        // innen el navigálódik az alkalmazás == ondisappearing (XF)
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
        }

        private async void Katt1_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new MessageDialog("Hello UWP!");
            await dialog.ShowAsync();
        }
    }
}
