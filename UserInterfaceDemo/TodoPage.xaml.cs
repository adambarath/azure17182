﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using WebStuff.Models;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace UserInterfaceDemo
{
    public sealed partial class TodoPage : Page
    {
        #region TODOITEMS

        static readonly ObservableCollection<TodoItem> tempDatabase = new ObservableCollection<TodoItem>
        {
            //new TodoItem { Description = "Kenyér" },
            //new TodoItem { Description = "USB töltő" },
            //new TodoItem { Description = "Táska" },
        };

        #endregion

        CloudStorageAccount acc;

        CloudBlobContainer todoContainer;
        CloudBlobClient blobClient;
        CloudBlockBlob fileBlob;

        CloudTableClient tableClient;
        CloudTable todoTable;

        public TodoPage()
        {
            this.InitializeComponent();

            acc = CloudStorageAccount.Parse(Constants.AzureConnectionString);

            blobClient = acc.CreateCloudBlobClient();
            todoContainer = blobClient.GetContainerReference("hdmi");
            fileBlob = todoContainer.GetBlockBlobReference("mindegy.txt");

            tableClient = acc.CreateCloudTableClient();
            todoTable = tableClient.GetTableReference("todo");

            TodoListBox.ItemsSource = tempDatabase;
        }

        private async Task UploadListAsync()
        {
            var jsonData = JsonConvert.SerializeObject(tempDatabase);
            await fileBlob.UploadTextAsync(jsonData);

            //var data = Encoding.UTF8.GetBytes(jsonData);
            //using (var stream = new MemoryStream(data))
            //    await fileBlob.UploadFromStreamAsync(stream);
        }

        private async Task RefreshList()
        {
            #region CloudBlob

            //await todoContainer.CreateIfNotExistsAsync();

            //if (!(await fileBlob.ExistsAsync()))
            //{
            //    return;
            //}

            //var jsonDown = await fileBlob.DownloadTextAsync();
            //try
            //{
            //    var tmp = JsonConvert.DeserializeObject<List<TodoItem>>(jsonDown);
            //    tempDatabase.Clear();
            //    foreach (var item in tmp)
            //    {
            //        tempDatabase.Add(item);
            //    }
            //    //TodoListBox.ItemsSource = tmp; // nincs szükség ilyenre, hisz az observablecollection kezeli a változásokat
            //}
            //catch { }

            #endregion

            #region Cloud Tables

            await todoTable.CreateIfNotExistsAsync();

            try
            {
                tempDatabase.Clear();

                var query = new TableQuery<TodoItem>();
                var token = new TableContinuationToken();
                // buta mód force-oljuk az egész tábla lekérdezését. (éles környezetben ilyet ne!)
                do
                {
                    var segment = await todoTable.ExecuteQuerySegmentedAsync(query, token); // egyszerre ezer elem
                    token = segment.ContinuationToken;
                    if (segment.Results.Any())
                    {
                        foreach (var segmentResult in segment.Results)
                        {
                            tempDatabase.Add(segmentResult);
                        }
                    }
                } while (token != null);
            }
            catch { }
            #endregion
        }

        private async void Refresh_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            await RefreshList();
        }

        private async void New_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            await RefreshList();

            var description = NewTxtBx.Text;
            if (!string.IsNullOrEmpty(description))
            {
                var id = new TodoItem();
                id.Description = description;
                tempDatabase.Add(id);

                #region Cloud Blob

                // await UploadListAsync();

                #endregion


                #region Cloud Tables

                var insertOperation = TableOperation.InsertOrMerge(id);
                await todoTable.ExecuteAsync(insertOperation);

                #endregion
            }

            await RefreshList();
        }

        private async void Delete_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            TodoItem entity = TodoListBox.SelectedItem as TodoItem;
            await RefreshList();
            if (entity != null)
            {
                var listItem = tempDatabase.FirstOrDefault(x => x.Id == entity.Id);
                tempDatabase.Remove(listItem);

                #region Cloud Blob

                //await UploadListAsync();

                #endregion

                #region Cloud Tables

                var insertOperation = TableOperation.Delete(listItem);
                await todoTable.ExecuteAsync(insertOperation);

                #endregion
            }
            await RefreshList();
        }

        private async void Edit_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            TodoItem entity = TodoListBox.SelectedItem as TodoItem;
            var description = EditTxtBx.Text;
            await RefreshList();
            if (entity != null)
            {
                var listItem = tempDatabase.FirstOrDefault(x => x.Id == entity.Id);
                listItem.Description = description;

                #region Cloud Blob

                //await UploadListAsync();

                #endregion

                #region Cloud Tables

                var insertOperation = TableOperation.InsertOrMerge(listItem);
                await todoTable.ExecuteAsync(insertOperation);

                #endregion
            }
        }
    }
}
