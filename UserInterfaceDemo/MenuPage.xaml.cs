﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace UserInterfaceDemo
{
    public sealed partial class MenuPage : Page
    {
        //private int _myProperty;
        //public int MyProperty
        //{
        //    get => _myProperty;
        //    set => _myProperty = value;
        //}

        public MenuPage()
        {
            this.InitializeComponent();
        }

        private void Navigate_UiDemo_Click(object sender, RoutedEventArgs eventArgs)
        {
            Frame.Navigate(typeof(MainPage));
        }

        private void Navigate_Todo_Click(object sender, RoutedEventArgs eventArgs)
        {
            Frame.Navigate(typeof(TodoPage));
        }


    }
}
