﻿using Microsoft.WindowsAzure.Messaging;
using System;
using System.Diagnostics;
using Windows.Networking.PushNotifications;

namespace UserInterfaceDemo
{
    public static class NotificationHelper
    {
        public static async void Init()
        {
            var channel
                = await PushNotificationChannelManager
                .CreatePushNotificationChannelForApplicationAsync();
            
            var hub = new NotificationHub("elteazure",
                "Endpoint=sb://elteazure.servicebus.windows.net/;SharedAccessKeyName=DefaultFullSharedAccessSignature;SharedAccessKey=aaWMpvRedcpxr0hrxM60YHO9ZVPEm4BJjcgUOmDbzzQ=");

            channel.PushNotificationReceived += Channel_PushNotificationReceived;

            // csak sör és update tagekről kérünk értesítést
            var result = await hub.RegisterNativeAsync(channel.Uri, new[] { "sör", "update" });
            if (result.RegistrationId != null)
            {
                Debug.WriteLine("Sikeres regisztracio: Notihub");
            }
        }

        private static void Channel_PushNotificationReceived
            (PushNotificationChannel sender, PushNotificationReceivedEventArgs args)
        {
            var data = args.ToastNotification.Data;
            var d = args.ToastNotification;
        }
    }
}
