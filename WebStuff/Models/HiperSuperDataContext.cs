﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace WebStuff.Models
{
    public class HiperSuperDataContext : DbContext
    {
        public HiperSuperDataContext(DbContextOptions options)
            : base(options)
        //: base("name=MS_TableConnectionString")
        {
            //régen: this.Database.EnableAutoMigrations = true;
        }

        public DbSet<TodoItem> TodoItems { get; set; }

        internal static void Initialize(HiperSuperDataContext context)
        {
            context.Database.EnsureCreated();

            if (context.TodoItems.Any())
            {
                return;
            }

            var todoItems = new TodoItem[] {
                new TodoItem { Description = "Kenyér" },
                new TodoItem { Description = "USB töltő" },
                new TodoItem { Description = "Táska" },
            };
            foreach (var item in todoItems)
            {
                context.TodoItems.Add(item);
            }
            context.SaveChanges();
        }
    }
}
