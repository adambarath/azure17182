﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebStuff.Models
{
    public class FibonacciEredmenyModel
    {
        public string Cim { get; set; }

        public IEnumerable<int> Szamok { get; set; }
    }
}
