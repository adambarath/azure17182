﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WebStuff.Models
{
    public class TodoItem :
         Microsoft.WindowsAzure.Storage.Table.TableEntity
        , INotifyPropertyChanged
    {
        public TodoItem()
        {
            Id = Guid.NewGuid().ToString();
            RowKey = Id;
            PartitionKey = "TodoItems";
            CreatedAt = DateTimeOffset.Now;

            // DateTime.
            //CreatedAt.Value.ToUnixTimeMilliseconds() emiatt szeretjük ezt használni
        }

        [Key]
        public string Id { get; set; }

        private string _description;
        [StringLength(16, ErrorMessage = "NEM")]
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                OnPropertyChanged(nameof(Description));
            }
        }

        // FONTOS, hogy a Tables nem tud System.DateTime-ot kezelni. Ezért használunk DateTimeOffset
        public DateTimeOffset? CreatedAt { get; set; }

        #region  INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));

            //if(PropertyChanged != null)
            //{
            //    PropertyChanged.Invoke(..., ...);
            //}
        }

        #endregion
    }
}
