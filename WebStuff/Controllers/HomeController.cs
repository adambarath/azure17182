﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.Azure.NotificationHubs;
using WebStuff.Models;

namespace WebStuff.Controllers
{
    public class HomeController : Controller
    {
        HiperSuperDataContext hiperSuperDataContext;

        CloudStorageAccount acc;

        CloudTableClient tableClient;
        CloudTable todoTable;

        public HomeController(HiperSuperDataContext hiperSuperDataContext)
        {
            this.hiperSuperDataContext = hiperSuperDataContext;

            acc = CloudStorageAccount.Parse(Constants.AzureConnectionString);
            tableClient = acc.CreateCloudTableClient();
            todoTable = tableClient.GetTableReference("todo");
        }

        #region TODOITEMS

        public IActionResult Create()
        {
            // olyan viewt adunk vissza
            // ami nem az action nevével megfeleltetett
            return View(viewName: "CreateTodoItem");
        }

        public async Task<IActionResult> CreateItem(TodoItem todoItem)
        {
            if (!ModelState.IsValid)
            {
                return View(viewName: "CreateTodoItem", model: todoItem);
            }
            else
            {
                #region Cloud Tables

                //var insertOperation = TableOperation.InsertOrMerge(todoItem);
                //await todoTable.ExecuteAsync(insertOperation);

                #endregion

                #region SQL

                hiperSuperDataContext.TodoItems.Add(todoItem);
                hiperSuperDataContext.SaveChanges();

                #endregion

                // notifications:
                var client =
                    NotificationHubClient.CreateClientFromConnectionString(
                            "Endpoint=sb://elteazure.servicebus.windows.net/;SharedAccessKeyName=DefaultFullSharedAccessSignature;SharedAccessKey=aaWMpvRedcpxr0hrxM60YHO9ZVPEm4BJjcgUOmDbzzQ=",
                            "elteazure"
                        );
                var notification = "<?xml version=\"1.0\" encoding=\"utf-8\"?><toast><visual><binding template=\"ToastText01\"><text id=\"1\">"
                    + "Uj tennivalo akadt: " + todoItem.Description
                    + "</text></binding></visual></toast>";

                await client.SendWindowsNativeNotificationAsync(notification, "update");
                // általános megvalósítás: client.SendNotificationAsync()

            }
            return View(viewName: "TodoItems", model: await RefreshList());
        }

        public async Task<IActionResult> TodoItems()
        {
            var items = await RefreshList();

            return View(items);
        }

        #region Cloud Table operations

        private async Task<List<TodoItem>> RefreshList()
        {
            var tempDatabase = new List<TodoItem>();


            var todoItems = hiperSuperDataContext.TodoItems.ToList();
            tempDatabase.AddRange(todoItems);



            //await todoTable.CreateIfNotExistsAsync();
            //try
            //{
            //    tempDatabase.Clear();

            //    var query = new TableQuery<TodoItem>();
            //    var token = new TableContinuationToken();
            //    // buta mód force-oljuk az egész tábla lekérdezését. (éles környezetben ilyet ne!)
            //    do
            //    {
            //        var segment = await todoTable.ExecuteQuerySegmentedAsync(query, token); // egyszerre ezer elem
            //        token = segment.ContinuationToken;
            //        if (segment.Results.Any())
            //        {
            //            foreach (var segmentResult in segment.Results)
            //            {
            //                tempDatabase.Add(segmentResult);
            //            }
            //        }
            //    } while (token != null);
            //}
            //catch { }

            return await Task.FromResult(tempDatabase);
        }

        #endregion

        #endregion

        #region nem érdekes 

        public IActionResult Fibonacci(int kezdoertek)
        {
            FibonacciEredmenyModel model = new FibonacciEredmenyModel();

            model.Cim = "Sziasztok!";
            model.Szamok = new List<int>() { kezdoertek, 1, 1, 2, 3 };

            return View(model);
        }

        public IActionResult Index()
        {
            //throw new Exception("tralalalal");

            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        #endregion
    }
}
