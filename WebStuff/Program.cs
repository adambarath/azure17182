﻿using System;
using System.Diagnostics;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using WebStuff.Models;

namespace WebStuff
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .UseApplicationInsights()
                .Build();

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var context = services.GetRequiredService<HiperSuperDataContext>();
                    HiperSuperDataContext.Initialize(context);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("An error occurred while seeding the database.");
                    Debug.WriteLine(ex);
                }
            }

            host.Run();
        }
    }
}
