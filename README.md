# Bevezetés
A tárgy célja, hogy áttekintést adjon a hallgatók részére a felhő alapú technológiákról és architektúráról az Azure rendszer bemutatásán keresztül. A félév során a hallgatók betekintést nyerhetnek a felhő alapú alkalmazások fejlesztésének és üzemeltetésének alapelveibe és meghatározó technológiáiba. A tárgy keretében bemutatásra kerül az IaaS, PaaS és SaaS üzemeltetési modell. Ezek kapcsán részletesen áttekintjük a különböző adattárolási megoldásokat (relációs, félstrukturált és bináris adatokhoz), sorra véve az egyes megoldások architekturális sajátosságait, s fejlesztői vonatkozásait is. Megvizsgáljuk, miként alakíthatjuk ki a felhőben a saját virtuális infrastrukturánkat, s hogyan kapcsolhatjuk azt hatékonyan össze lokális egy lokális adatközponttal, vagy számtalan hordozható klienssel, illetve, hogy milyen alapokat kínál az Azure különböző platformokra készült alkalmazások számára. Áttekintjük, a felhő által kínált lehetőségek nagy számításigényű műveletekhez, vagy akár a korlátozott számítási kapacitású eszközök összefogásához, kiszolgálásához (IoT, Mobil eszközök). A tárgy szerves részét képezik a gyakorlati foglalkozások, melyek lehetőséget biztosítanak a hallgatók számára, hogy az előadáson tanultakat maguk is kipróbálhassák.

# További információ

http://cs.inf.elte.hu

# Gyakorlat vezető

Baráth Ádám
adambarath@msn.com