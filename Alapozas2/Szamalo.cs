﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Alapozas2
{
    //delegate void EzEgyDelegalt();

    public class Szamalo
    {
        private int i;  // java final == readonly

        //EzEgyDelegalt ez;

        public Szamalo()
            : this(15)  // ugyanazon példány konstruktorainak a láncolása
        {
            //super(15); // Java-ban így
            //this(15);  // Java-ban így
        }

        public Szamalo(int i)
            : base() // ősosztály konstruktorát hívja 
                     // -> paraméter nélküli ős konstruktorát nem kell explicit meghívni
        {
            this.i = i;
        }

        public Szamalo(string szam)
        {
            //i = szam; // nem lehetséges, erősen típusosak vagyunk
            //i = int.Parse(szam);    // throws invalidcastexception

            if (int.TryParse(szam, out i))
            {
                Console.WriteLine("sikeres konvertálás");
            }
            else
            {
                Console.WriteLine(":(");
            }

            //(int)double.Parse(szam);
            //i = (int)System.Math.Round(5.5d); // kerekítés

            //string műveletek: szam.Substring();
        }

        public IEnumerable<int> FibonacciGyorsan(int m)
        {
            for (int n = 0; n < m; n++)
            {
                System.Threading.Thread.Sleep(10);
                int a = 0;
                int b = 1;
                // In N steps compute Fibonacci sequence iteratively.
                for (int i = 0; i < n; i++)
                {
                    int temp = a;
                    a = b;
                    b = temp + b;
                }
                yield return a;
            }
        }

        public IEnumerable<int> FibonacciLassan(int m)
        {
            List<int> list = new List<int>();   // előre készít egy listát
            for (int n = 0; n < m; n++)
            {
                System.Threading.Thread.Sleep(10);
                int a = 0;
                int b = 1;
                // In N steps compute Fibonacci sequence iteratively.
                for (int i = 0; i < n; i++)
                {
                    int temp = a;
                    a = b;
                    b = temp + b;
                }
                list.Add(a);
            }
            return list;
        }
    }
}
