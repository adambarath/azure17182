﻿using System;
using System.Diagnostics;

namespace Alapozas2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello C#!");

            //Szamalo szamalo = new Szamalo();

            //for (int i = 0; i < 5; i++)
            {
                string szam = Console.ReadLine();
                var sz = new Szamalo(szam);
            }

            Szamalo fibonacciSzamlalo = new Szamalo();

            //System.Diagnostics.Stopwatch
            Stopwatch sw = new Stopwatch();
            sw.Start(); // időzítőt indítunk a mérésre

            #region gyorsan

            sw.Restart();
            int counter = 0;
            foreach (var lepes in fibonacciSzamlalo.FibonacciGyorsan(500))
            {
                if (counter >= 10)
                {
                    break;
                }
                Console.WriteLine(lepes);
                counter++;
            }
            Console.WriteLine("eltelt idő:" + sw.ElapsedMilliseconds);

            #endregion

            #region lassan

            sw.Restart();
            counter = 0;
            foreach (var lepes in fibonacciSzamlalo.FibonacciLassan(500))
            {
                if (counter >= 10)
                {
                    break;
                }
                Console.WriteLine(lepes);
                counter++;
            }
            Console.WriteLine("eltelt idő:" + sw.ElapsedMilliseconds);

            #endregion

            Console.ReadKey();
        }
    }
}
