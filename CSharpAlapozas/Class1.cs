﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alapozas
{
    public class Class1
    {
        public static void Main(string[] args)
        {
            int p = 5;

            OutOfSpace.Hello(); // tehát működik // global::OutOfSpace
            Console.WriteLine("" + InOutDelegate(10, ref p));
            Console.WriteLine(p);

            Console.ReadKey();
        }

        static int InOutDelegate(int a, ref int b)
        {
            ++b;
            return a;
        }
    }
}
